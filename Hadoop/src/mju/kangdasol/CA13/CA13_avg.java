package mju.kangdasol.CA13;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

public class CA13_avg {
    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {
		@Override
		public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
			String line = value.toString(); //잘라낸 한 라인(고객 1명의 정보).
			List extraList = new ArrayList<>(); //나이 제외한 나머지 정보들을 담을 리스트.
			
			StringTokenizer token = new StringTokenizer(line);
			int ageI = Integer.parseInt(token.nextToken("_")); //고객의 나이(int)
			ageI = ageI-ageI%10; //고객의 나이대.
			String ageS = String.valueOf(ageI);
			Text ageT = new Text();
			ageT.set(ageS); //고객의 나이대를 text로 변환.
			while(token.hasMoreTokens()){ 
				//나이 제외한 나머지 정보들 add. 인덱스별 정보:0(성별), 1(지역), 2(제품군), 3(접속시간), 4(월별구매횟수), 5(금액대)
				extraList.add(token.nextToken("_"));
			}
			//금액대.
			Text cost = new Text();
			cost.set(extraList.get(5).toString());
			//월별구매횟수
			Text buy_count = new Text();
			buy_count.set(extraList.get(4).toString());
			//접속시간대
			Text time = new Text();
			time.set(extraList.get(3).toString());
			
			output.collect(ageT, cost); 
			output.collect(ageT, buy_count); 
			output.collect(ageT, time);
		}
    }
	
	   public static class Reduce extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
	     public void reduce(Text key, Iterator<Text> values, 
	    		 OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
	    	 //(나이,금액대 평균)
	    	 /*
	    	  * 1. values를 text를 int로 바꾼다.
	    	  * 2. 평균을 구한다.(소수점은 버림) 이를 value로 한다.
	    	  * 3. 출력*/
	    	 int sum = 0;
	    	 int avg = 0;
	    	 int sum_count = 0;
	    	 int avg_count = 0;
	    	 int sum_time = 0;
	    	 int avg_time = 0;
	    	 Text avgT = new Text();
	    	 Text avgT_count = new Text();
	    	 Text avgT_time = new Text();
	         
	    	 List<Integer> costL = new ArrayList<>(); //금액대
	    	 List<Integer> countL = new ArrayList<>(); //월별구매횟수
	    	 List<Integer> timeL = new ArrayList<>(); //접속시간
	    	 while(values.hasNext()){
	    		 costL.add(Integer.parseInt(values.next().toString()));
	    		 countL.add(Integer.parseInt(values.next().toString()));
	    		 timeL.add(Integer.parseInt(values.next().toString()));
	    	 }
	    	 Iterator<Integer> costI = costL.iterator(); //text iterator를 integer iterator으로 변환
	    	 Iterator<Integer> countI = countL.iterator();
	    	 Iterator<Integer> timeI = timeL.iterator();
	    	 while(costI.hasNext()){
	    		 sum += costI.next();
	    	 }
	    	 while(countI.hasNext()){
	    		 sum_count += countI.next(); 
	    	 }
	    	 while(timeI.hasNext()){
	             sum_time += timeI.next();
	          }
	    	 avg = sum/costL.size(); //평균(int)
	    	 avg_count = sum_count/countL.size();
	    	 avg_time = sum_time/timeL.size();
	         
	    	 avgT.set(String.valueOf(avg)); //평균(text)
	    	 avgT_count.set(String.valueOf(avg_count));
	    	 avgT_time.set(String.valueOf(avg_time));
	    	 
	    	 output.collect(key, avgT); //금액대
	    	 output.collect(key, avgT_count); //월별구매횟수
	    	 output.collect(key, avgT_time); //접속시간대 평균
	     }
	   }
	   
	public static void main(String[] args) throws IOException {
		JobConf conf = new JobConf(CA13_avg.class);
		conf.setJobName("CA13_avg");
	     conf.setOutputKeyClass(Text.class);
	     conf.setOutputValueClass(Text.class);
	
	     conf.setMapperClass(Map.class);
	     conf.setReducerClass(Reduce.class);
	
	     conf.setInputFormat(TextInputFormat.class);
	     conf.setOutputFormat(TextOutputFormat.class);
	
	     FileInputFormat.setInputPaths(conf, new Path(args[0]));
	     FileOutputFormat.setOutputPath(conf, new Path(args[1]));
	
	     JobClient.runJob(conf);
	}
}
