package mju.kangdasol.CA13;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import mju.kangdasol.CA13.CA13_avg.Map;
import mju.kangdasol.CA13.CA13_avg.Reduce;

public class CA13_max {
    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, 
	Text, Text> {
    	@Override
    	public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, 
    			Reporter reporter) throws IOException {
    		String line = value.toString();
    		List customer_info = new ArrayList<>();
    		StringTokenizer token = new StringTokenizer(line);
			while(token.hasMoreTokens()){
				//인덱스별 정보:0(나이), 1(성별), 2(지역), 3(카테고리), 4(시간대), 5(월별구매횟수), 6(금액대)
				customer_info.add(token.nextToken("_"));
			}
			Text category = new Text();
			category.set(customer_info.get(3).toString());
			Text age = new Text();
			age.set(customer_info.get(0).toString());
			output.collect(category, age);
			
			Text gender = new Text();
			gender.set(customer_info.get(1).toString());
			output.collect(category, gender);
    	}
    }

    public static class Reduce extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
		public void reduce(Text key, Iterator<Text> values, 
				OutputCollector<Text, Text> output, Reporter reporter) throws IOException {

			//text로 들어온 고객의 나이대를 integer로 변경함.
			List<Integer> ageL = new ArrayList<>();
			List<String> genderL = new ArrayList<>();
			while(values.hasNext()){
				ageL.add(Integer.parseInt(values.next().toString()));
				genderL.add(values.next().toString());
			} 
			
			//키는 나이대, value는 빈도수인데 일단 0으로 초기화.
			HashMap<Integer, Integer> age_hashMap = new HashMap<>();
			age_hashMap.put(10, 0);
			age_hashMap.put(20, 0);
			age_hashMap.put(30, 0);
			age_hashMap.put(40, 0);
			age_hashMap.put(50, 0);
			age_hashMap.put(60, 0);
			
			//나이 list를 돌면서 나이대를 판단하여 해당 hashMap value를 증가시켜준다.
			Iterator<Integer> ageI = ageL.iterator();
			int v1=0, v2=0, v3=0, v4=0, v5=0, v6=0;
			int temp=0;
			while(ageI.hasNext()){
				temp = ageI.next();
				if(temp>=10&&temp<20){
					age_hashMap.put(10, ++v1);
				}else if(temp>=20&&temp<30){
					age_hashMap.put(20, ++v2);
				}else if(temp>=30&&temp<40){
					age_hashMap.put(30, ++v3);
				}else if(temp>=40&&temp<50){
					age_hashMap.put(40, ++v4);
				}else if(temp>=50&&temp<60){
					age_hashMap.put(50, ++v5);
				}else if(temp>=60){
					age_hashMap.put(60, ++v6);
				}
			}

			//제품군 별로 구매한 나이 대 빈도 수 출력됨. Car {10=0,20=1,30=0}
			Text age_countT = new Text();
			age_countT.set(String.valueOf(age_hashMap));
			output.collect(key, age_countT);
			
			//성별
			HashMap<String, Integer> gender_hashMap = new HashMap<>();
			gender_hashMap.put("Female", 0);
			gender_hashMap.put("Male", 0);
			
			//성별 list를 돌면서 나이대를 판단하여 해당 hashMap value를 증가시켜준다.
			Iterator<String> genderI = genderL.iterator();
			int f=0, m=0;
			String temp1="";
			while(genderI.hasNext()){ //values는 [F,M,F,F,M]이렇게 저장되어 있음
				temp1=genderI.next();
				if(temp1.equals("F")){
					gender_hashMap.put("Female", ++f);
				}else if(temp1.equals("M")){
					gender_hashMap.put("Male", ++m);
				}
			}

			//제품군 별로 구매한 성별 빈도 수 출력됨. Car {Female=0,Male=1}
			Text gender_countT = new Text();
			gender_countT.set(String.valueOf(gender_hashMap));
			output.collect(key, gender_countT);
		}
    }

	public static void main(String[] args) throws IOException {
		JobConf conf = new JobConf(CA13_max.class);
		conf.setJobName("CA13_max");
	     conf.setOutputKeyClass(Text.class);
	     conf.setOutputValueClass(Text.class);
	
	     conf.setMapperClass(Map.class);
	     conf.setReducerClass(Reduce.class);
	
	     conf.setInputFormat(TextInputFormat.class);
	     conf.setOutputFormat(TextOutputFormat.class);
	     FileInputFormat.setInputPaths(conf, new Path(args[0]));
	     FileOutputFormat.setOutputPath(conf, new Path(args[1]));
	
	     JobClient.runJob(conf);
	}

}
